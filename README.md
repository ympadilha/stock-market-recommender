# LogMeIn front-end coding challenge

Since this is a React app created with CRA I'm leaving the default README.md of CRA at the bottom of this readme.
  
## Stock Market Recommender

### Technical Requirements

Since we currently do not have an API we're mocking the values just as the specification described.
I have simulated calls to 4 distinct endpoints.

#### Info about the endpoints

- Stock price endpoint -> Fetch `Stock prices` based on a `Stock symbol` and a list of `Dates`.
The API should return a list of `StockPrices` that consists in a `tuple` of `stockValue` and `date`.

- Social media count endpoint -> Fetch `Social media posts quantity of a given day` based on a `Stock symbol`,
 a list of `Different Social Medias` and list of `Dates`.
 The API should return a list of `Social media posts quantity` that consists in a `tuple` of `Social media posts quantity`
 and `date`.
 
- Recommendations endpoint -> Fetch `Recommendation [BUY, SELL, KEEP]` based on a list of `Stock prices` and
 `Dates`. The API should return a list of `Recommendations` that consists in a `tuple` of `recommendation` and
 `date`.
 
 - TopSocialMediaPosts endpoint -> Fetch `The 2 top rated posts` based on a list of `Stock symbol` and the current `date`.
 The API should return a list containing `2 top rated posts` that consists of `title`, `description`, `hashTag`, `imageUrl`
 and `agent_smith_url`.
 
Of course the API returns nothing and I mocked with some random values all of the responses. It should be really
easy to change the mocked values to the API's values when the backend is ready.
 
Please check the files inside the `requesters` folder for more information. I left comments where the code
should be changed.
 
#### The code must be modern and easy to maintain
 
I decided to create the APP with React for a couple of reasons:
 
 1) Most used frontend lib out there (I'd paste some links here but you can just easily search google for that)
 2) I am reasonably comfortable working with it.
 3) Typescript supported and I love strong typed.
 
#### Your code must allow new features to be added easily

Also here we find that React is a good choice, the app updated easily thanks to React's built-in
smart re-render system (ShadowDOM diffs optimization).
Every time new information comes in or parts of it are changed the APP will re-render.

#### It must be tested so that there are no surprises in demos.

That's why I am using [Jest](https://jestjs.io/) to test. It comes out of the box with Create React App (CRA)
and is really nice to use. I tested as much as I can and tried to figure out every cenario. Both bad cenarios and good cenarios.

### Feature requirements

There's only 30 minutes left to complete the challenge and I completely forgot about this part. Well...

What I am thinking now to solve this is to call the `API` to see which `algorithms` are available and display them to
the user. Based on the displayed `algorithm` the view would change and more `inputs` would be added to the form so we'd
send complementary information as the `algorithms` need.

### Visual Requirements

Just using pure CSS and organizing it using [BEM](http://getbem.com/).

- The user can type anything, literally, I am not validating any kind of `Stock symbol`.
- The user can click the three available social medias to enable/disable them. The 
API receives which `Social Medias` are being used since we send them to the backend.
- The `Time window` is a `number input`. Unfortunately I had no time to put some arrows for the user to sum/subtract 1 day.

There are placeholders for when there are no information yet requested.

I didn't implement a not `found/found no matches` for the view.

There is an `error alert` at the top of the application that will show up in case there's an `exception/error` in the `API`.
To trigger an error one could simply change the URL we are requesting to something that doesn't exist (also since I am sending
get requests to `placekitten.com` if their application goes offline or responds `500` we'll get an error!). 

#### Visualization table

I implemented a table to show the information in a neat way, besides that there's a problem that when a huge `time window`
is asked the DOM would render a lot of `tr` and it would be bad. In the future a better idea would be to do some sort of
lazy load of the information, it could be done by hitting the API when the user reaches the bottom of the table and
mounting the `HTML` of only the visible elements.

### Optional Challenges

Some of the optional challenges have been implemented, these are:

- Display the 2 `top social media` posts at the bottom. A request has been mocked for that.
- Allow used to add/remove social media service from system. Just as I described above the user can toggle the social medias.

### Running, testing and etc.

For information about running locally the app and running the tests you can check the section below. It's pretty simple.

### Known bugs

- Snapshots are being generate every time for some components since we have random values being generated. What could
be done to fix this is to mock all the generated requests in these tests (just like I did with some other ones) and to stop
generating this random info.
- Anything that the user does in the APP will a bunch of `http requests` to the API. That's terrible but I had no time to fix it.
What I'd do to fix it is add a button and only when the user would press this button then the `requests` would happen. (I 
even have the layout for this ready!)
- (not a bug but...) I didn't implement at all the part `Feature requirements`. Honestly I completely forgot about it and I have no more time to do it.

# CRA default's README.md
 
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
