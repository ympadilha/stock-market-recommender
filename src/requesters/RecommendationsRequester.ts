import axios from 'axios';
import { Moment } from 'moment';
import generators from './randomGenerators';
import { errorFunction, RecommendationByDay, RequestUrls } from "./CommonDefinitions";

export const RecommendationsRequester = (
  stockPricesForDates: string[],
  socialMediaPostQuantitiesForDates: number[],
  dates: Moment[]
): Promise<RecommendationByDay[]> => {
  return axios
    .get(RequestUrls.Recommendations, {
      params: {
        stockPricesForDates,
        socialMediaPostQuantitiesForDates,
        dates,
      }
    })
    .then(response => {
      /* Implement code here, this is just a mock from now */
      return dates.map<RecommendationByDay>(date => ({
        date,
        recommendation: generators.randomRecommendation(),
      }));
    })
    .catch(errorFunction('RecommendationsRequester'));
};
