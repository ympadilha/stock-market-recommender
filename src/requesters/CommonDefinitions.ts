import { Moment } from "moment";
import { Recommendation } from "../containers";

export const errorFunction = <TResult = never>(errorLabel: string) => (
	error: (reason: any) => TResult | PromiseLike<TResult>
) => {
	/* It is possible to call any error/exception service for logging */
	throw Error(`${errorLabel} Error`);
};

export interface RecommendationByDay {
	date: Moment;
	recommendation: Recommendation;
}

export interface SocialMediaCountByDay {
	date: Moment;
	numberOfPosts: number;
}

export interface StockPriceByDay {
	date: Moment;
	stockPrice: string;
}

export interface TopSocialMediaPostsOfToday {
	title: string;
	description: string;
	hashTag: string;
	imageUrl: string;
}

/**
 * insert URLs of the API here when backend is done
 */
export enum RequestUrls {
	Recommendations = 'http://placekitten.com/g/200/300',
	SocialMediasCount = 'http://placekitten.com/g/200/300',
	StockPrice = 'http://placekitten.com/g/200/300',
	TopSocialMediaPosts = 'http://placekitten.com/g/200/300',
}


