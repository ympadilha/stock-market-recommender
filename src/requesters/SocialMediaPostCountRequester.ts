import { Moment } from "moment";
import axios from "axios";
import generators from "./randomGenerators";
import { errorFunction, RequestUrls, SocialMediaCountByDay } from "./CommonDefinitions";

export const SocialMediaPostCountRequester = (
	stockSymbol: string,
	socialMedias: string[],
	dates: Moment[]
): Promise<SocialMediaCountByDay[]> => {
	return axios
		.get(RequestUrls.SocialMediasCount, {
			params: {
				stockSymbol,
				socialMedias,
				dates,
			}
		})
		.then(response => {
			/* Implement code here, this is just a mock from now */
			return dates.map<SocialMediaCountByDay>(date => ({
				date,
				numberOfPosts: generators.randomSocialMediaPostQuantity(),
			}));
		})
		.catch(errorFunction('SocialMediaCountRequester'));
};

