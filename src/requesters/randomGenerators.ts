import { TopSocialMediaPostsOfToday } from "./CommonDefinitions";
import { Recommendation } from "../containers/App/reducers";

/**
 * TODO as soon as you don't need random numbers anymore this file can be deleted
 */
const randomRecommendation = (): Recommendation => {
  const random = Math.random();
  if (random < 0.3) return Recommendation.BUY;
  if (random < 0.6) return Recommendation.SELL;
  return Recommendation.KEEP;
};

const randomStockPrice = (): string => Math.ceil(1000 * (Math.random() + 1)).toFixed(2);
const randomSocialMediaPostQuantity = (): number => Math.ceil(100 * (Math.random() + 1));

const mockedTopPosts = (): TopSocialMediaPostsOfToday[] => {
  return [
    { title: 'Agent Smith', description: 'Gotta duplicate, more. Mitosis', hashTag: '@agentpotato', imageUrl: 'agent_smith_url'},
    { title: 'Mr. Anderson', description: 'Dodge bullets, eat hamburguer, cheeseburguer', hashTag: '@andy', imageUrl: 'mr_anderson_url'}
  ]
};

export default { randomRecommendation, randomStockPrice, randomSocialMediaPostQuantity, mockedTopPosts };
