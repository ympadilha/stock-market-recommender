import { Moment } from "moment";
import axios from "axios";
import { errorFunction, RequestUrls, StockPriceByDay } from "./CommonDefinitions";
import generators from "./randomGenerators";

export const StockPriceRequester = (stockSymbol: string, dates: Moment[]): Promise<StockPriceByDay[]> => {
	return axios
		.get(RequestUrls.StockPrice, {
			params: {
				stockSymbol,
				dates,
			}
		})
		.then(response => {
			/* Implement code here, this is just a mock from now */
			return dates.map<StockPriceByDay>(date => ({
				date,
				// TODO to fixed is not good here, think something better
				stockPrice: generators.randomStockPrice(),
			}));
		})
		.catch(errorFunction('StockPriceRequester'));
};

