import axios from "axios";
import { errorFunction, RequestUrls, TopSocialMediaPostsOfToday } from "./CommonDefinitions";
import moment from "moment";
import generators from "./randomGenerators";

export const TopSocialMediaPostsRequester = (stockSymbol: string): Promise<TopSocialMediaPostsOfToday[]> => {
	return axios
		.get(RequestUrls.TopSocialMediaPosts, {
			params: {
				stockSymbol,
				date: moment.utc(),
			}
		})
		.then(response => {
			/* Implement code here, this is just a mock from now */
			return generators.mockedTopPosts();
		})
		.catch(errorFunction('TopSocialMediaPostsRequester'));
};
