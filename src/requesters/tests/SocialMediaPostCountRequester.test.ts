import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import generators from "../randomGenerators";
import { RequestUrls } from "../CommonDefinitions";
import { SocialMediaPostCountRequester } from "../SocialMediaPostCountRequester";
import { mockedDates, stockSymbol } from "./CommonMocks";

describe('SocialMediaCountRequester', () => {
	const socialMedias = ['Facebook'];

	it('SHOULD return correct array WHEN api does respond', async () => {
		const axiosMock = new MockAdapter(axios);
		const response = {};
		generators.randomSocialMediaPostQuantity = jest.fn().mockReturnValue(200);

		axiosMock.onGet(RequestUrls.Recommendations).reply(200, response);

		const socialMediaPostsCountByDay = await SocialMediaPostCountRequester(stockSymbol, socialMedias, mockedDates);

		expect(socialMediaPostsCountByDay[0].date).toEqual(mockedDates[0]);
		expect(socialMediaPostsCountByDay[0].numberOfPosts).toEqual(200);
	});

	it('SHOULD throw error with correct message WHEN api does not respond', async () => {
		const axiosMock = new MockAdapter(axios);
		const response = {};

		axiosMock.onGet(RequestUrls.Recommendations).reply(500, response);

		try {
			await SocialMediaPostCountRequester(stockSymbol, socialMedias, mockedDates);
		} catch (error) {
			expect(error.message).toEqual('SocialMediaCountRequester Error');
		}
	});
});

