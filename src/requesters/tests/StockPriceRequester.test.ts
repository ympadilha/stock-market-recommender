import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import generators from "../randomGenerators";
import { RequestUrls } from "../CommonDefinitions";
import { mockedDates, stockSymbol } from "./CommonMocks";
import { StockPriceRequester } from "../StockPriceRequester";

describe('StockPriceRequester', () => {
	it('SHOULD return correct array WHEN api does respond', async () => {
		const axiosMock = new MockAdapter(axios);
		const response = {};
		generators.randomStockPrice = jest.fn().mockReturnValue('12.00');

		axiosMock.onGet(RequestUrls.Recommendations).reply(200, response);

		const stockPriceByDay = await StockPriceRequester(stockSymbol, mockedDates);

		expect(stockPriceByDay[0].date).toEqual(mockedDates[0]);
		expect(stockPriceByDay[0].stockPrice).toEqual('12.00');
	});

	it('SHOULD throw error with correct message WHEN api does not respond', async () => {
		const axiosMock = new MockAdapter(axios);
		const response = {};

		axiosMock.onGet(RequestUrls.Recommendations).reply(500, response);

		try {
			await StockPriceRequester(stockSymbol, mockedDates);
		} catch (error) {
			expect(error.message).toEqual('StockPriceRequester Error');
		}
	});
});
