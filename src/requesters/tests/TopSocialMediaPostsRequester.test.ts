import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import generators from "../randomGenerators";
import { RequestUrls } from "../CommonDefinitions";
import { stockSymbol } from "./CommonMocks";
import { TopSocialMediaPostsRequester } from "../TopSocialMediaPostsRequester";

describe('StockPriceRequester', () => {
	it('SHOULD return correct array WHEN api does respond', async () => {
		const axiosMock = new MockAdapter(axios);
		const response = {};
		generators.randomStockPrice = jest.fn().mockReturnValue('12.00');

		axiosMock.onGet(RequestUrls.Recommendations).reply(200, response);

		const twoTopPosts = await TopSocialMediaPostsRequester(stockSymbol);

		expect(twoTopPosts).toEqual(generators.mockedTopPosts());
	});

	it('SHOULD throw error with correct message WHEN api does not respond', async () => {
		const axiosMock = new MockAdapter(axios);
		const response = {};

		axiosMock.onGet(RequestUrls.Recommendations).reply(500, response);

		try {
			await TopSocialMediaPostsRequester(stockSymbol);
		} catch (error) {
			expect(error.message).toEqual('TopSocialMediaPostsRequester Error');
		}
	});
});
