import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import moment from 'moment';
import generators from '../randomGenerators';
import { mockedDates } from "./CommonMocks";
import { RequestUrls } from "../CommonDefinitions";
import { RecommendationsRequester } from "../RecommendationsRequester";
import { Recommendation } from "../../containers/App/reducers";

describe('RecommendationsRequester', () => {
  it('SHOULD return correct array WHEN api does respond', async () => {
    const axiosMock = new MockAdapter(axios);
    const response = {};
    generators.randomRecommendation = jest.fn().mockReturnValue(Recommendation.BUY);

    const mockedStockPrices = ['15.12', '11.10'];
    const mockedSocialMediaPostQuantities = [1234, 4321];

    axiosMock.onGet(RequestUrls.Recommendations).reply(200, response);

    const recommendationsByDay = await RecommendationsRequester(
      mockedStockPrices,
      mockedSocialMediaPostQuantities,
      mockedDates
    );

    expect(recommendationsByDay[0].date).toEqual(mockedDates[0]);
    expect(recommendationsByDay[0].recommendation).toEqual(Recommendation.BUY);
  });

  it('SHOULD throw error with correct message WHEN api does not respond', async () => {
    const axiosMock = new MockAdapter(axios);
    const response = {};

    const mockedStockPrices = ['15.12', '11.10'];
    const mockedSocialMediaPostQuantities = [1234, 4321];
    const mockedDates = [moment('05/12/2019', 'MM/DD/YYYY')];

    axiosMock.onGet(RequestUrls.Recommendations).reply(500, response);

    try {
      await RecommendationsRequester(mockedStockPrices, mockedSocialMediaPostQuantities, mockedDates);
    } catch (error) {
      expect(error.message).toEqual('RecommendationsRequester Error');
    }
  });
});
