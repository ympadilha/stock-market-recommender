import { connect } from 'react-redux';
import { App } from '../../components/App';
import { Dispatch } from 'redux';
import { Moment } from 'moment';
import { startFetchingRecommendations, startFetchingTopSocialMediaPosts } from './actionCreators';
import { AppState } from '../../redux/store';

const mapStateToProps = (state: AppState) => ({
  recommendations: state.recommendationReducer.recommendations,
  topPosts: state.topSocialMediaPostsReducer.topPosts,
  alertIsVisible: state.alertReducer.isVisible,
  recommendationsLoading: state.recommendationReducer.recommendationsLoading,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  startFetchingRecommendations: (stockSymbol: string, socialMedias: string[], dates: Moment[]) =>
    dispatch(startFetchingRecommendations(stockSymbol, socialMedias, dates)),
  startFetchingTopSocialMediaPosts: (stockSymbol: string) => dispatch(startFetchingTopSocialMediaPosts(stockSymbol)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
