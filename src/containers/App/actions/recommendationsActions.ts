import { Moment } from 'moment';
import { FullStockRecommendation } from "../reducers";

/* Recommendations request */
export enum FetchRecommendationsActionNames {
  FETCHING = 'RECOMMENDATIONS_FETCHING',
  SUCCEDED = 'RECOMMENDATIONS_SUCCEEDED',
}

export interface StartFetchingRecommendations {
  type: FetchRecommendationsActionNames.FETCHING;
  payload: {
    stockSymbol: string;
    socialMedias: string[];
    dates: Moment[];
    recommendationsLoading: true;
  };
}

export interface SuccededFetchingRecommendations {
  type: FetchRecommendationsActionNames.SUCCEDED;
  payload: {
    recommendations: FullStockRecommendation[];
    recommendationsLoading: false;
  };
}

export type RecommendationActions = StartFetchingRecommendations | SuccededFetchingRecommendations;
