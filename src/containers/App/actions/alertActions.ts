/* Show alert */
export enum AlertActionNames {
	SHOW = 'ALERT_SHOW',
	HIDE = 'ALERT_HIDE',
}

export interface ShowAlert {
	type: AlertActionNames.SHOW;
}

export interface HideAlert {
	type: AlertActionNames.HIDE;
}

export type AlertActions = ShowAlert | HideAlert;
