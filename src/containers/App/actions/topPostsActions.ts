import { TopSocialMediaPostsOfToday } from "../../../requesters/CommonDefinitions";

export enum FetchTopSocialMediaPostsActionNames {
	FETCHING = 'TOP_SOCIAL_MEDIA_POSTS_FETCHING',
	SUCCEDED = 'TOP_SOCIAL_MEDIA_POSTS_SUCCEEDED',
}

export interface StartFetchingTopSocialMediaPosts {
	type: FetchTopSocialMediaPostsActionNames.FETCHING;
	payload: {
		stockSymbol: string;
	};
}

export interface SuccededFetchingTopSocialMediaPosts {
	type: FetchTopSocialMediaPostsActionNames.SUCCEDED;
	payload: {
		topPosts: TopSocialMediaPostsOfToday[];
	};
}

export type SocialMediaPostsActions = StartFetchingTopSocialMediaPosts | SuccededFetchingTopSocialMediaPosts;
