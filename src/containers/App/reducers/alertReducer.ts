import { Reducer } from 'redux';
import { AlertActionNames, AlertActions } from "../actions";

interface AlertState {
  isVisible: boolean;
}

const alertInitialState: AlertState = {
  isVisible: false,
};

export const alertReducer: Reducer<AlertState, AlertActions> = (
  state: AlertState = alertInitialState,
  action: AlertActions
): AlertState => {
  switch (action.type) {
    case AlertActionNames.SHOW:
      return { ...state, isVisible: true };
    case AlertActionNames.HIDE:
      return { ...state, isVisible: false };
  }

  return state;
};
