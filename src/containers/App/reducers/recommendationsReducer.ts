import { Reducer } from 'redux';
import { Moment } from 'moment';
import { FetchRecommendationsActionNames, RecommendationActions } from "../actions";

export enum Recommendation {
  BUY = 'Buy',
  SELL = 'Sell',
  KEEP = 'Keep',
}

export interface FullStockRecommendation {
  date: Moment;
  numberOfPosts: number;
  price: string;
  recommendation: Recommendation;
}

export interface RecommendationState {
  recommendations: FullStockRecommendation[];
  recommendationsLoading: boolean;
}

const recommendationInitialState: RecommendationState = {
  recommendations: [],
  recommendationsLoading: false,
};

export const recommendationReducer: Reducer<RecommendationState, RecommendationActions> = (
  state: RecommendationState = recommendationInitialState,
  action: RecommendationActions
): RecommendationState => {
  switch (action.type) {
    case FetchRecommendationsActionNames.SUCCEDED:
      return action.payload;
    case FetchRecommendationsActionNames.FETCHING:
      return { ...state, recommendationsLoading: action.payload.recommendationsLoading };
  }

  return state;
};
