import { Reducer } from "redux";
import { TopSocialMediaPostsOfToday } from "../../../requesters";
import { FetchTopSocialMediaPostsActionNames, SocialMediaPostsActions } from "../actions";

export interface TopSocialMediaPostsState {
	topPosts: TopSocialMediaPostsOfToday[]
}

const topSocialMediaPostsInicialState: TopSocialMediaPostsState = {
	topPosts: [],
};

export const topSocialMediaPostsReducer: Reducer<TopSocialMediaPostsState, SocialMediaPostsActions> = (
	state: TopSocialMediaPostsState = topSocialMediaPostsInicialState,
	action: SocialMediaPostsActions,
): TopSocialMediaPostsState => {
	switch (action.type) {
		case FetchTopSocialMediaPostsActionNames.SUCCEDED:
			return action.payload;
		case FetchTopSocialMediaPostsActionNames.FETCHING:
			return state;
	}

	return state;
};
