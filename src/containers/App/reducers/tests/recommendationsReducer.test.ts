import moment from 'moment';
import { mockedRecommendations } from "../../../../components/App/tests/mockedData";
import { recommendationReducer, RecommendationState } from "../recommendationsReducer";
import { startFetchingRecommendations, succededFetchingRecommendations } from "../../actionCreators";

describe('App Reducer', () => {
  it('SHOULD not change state WHEN action received is fetching', () => {
    const testDate = moment('05/12/2019', 'MM/DD/YYYY');

    const mockedInitialState: RecommendationState = {
      recommendations: mockedRecommendations,
      recommendationsLoading: true,
    };

    const action = startFetchingRecommendations('abc', ['Facebook'], [testDate]);

    const newState = recommendationReducer(mockedInitialState, action);

    expect(newState).toEqual(mockedInitialState);
  });

  it('SHOULD change state WHEN action received is succeeded ', () => {
    const mockedRecommendationsChanged = mockedRecommendations;
    mockedRecommendationsChanged[0].price = '33.55';
    mockedRecommendationsChanged[0].numberOfPosts = 333;

    const mockedInitialState: RecommendationState = {
      recommendations: mockedRecommendations,
      recommendationsLoading: true,
    };
    const expectedState: RecommendationState = {
      recommendations: mockedRecommendationsChanged,
      recommendationsLoading: false,
    };

    const action = succededFetchingRecommendations(mockedRecommendationsChanged);

    const newState = recommendationReducer(mockedInitialState, action);

    expect(newState).toEqual(expectedState);
  });
});
