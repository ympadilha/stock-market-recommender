import moment from 'moment';
import {
  FetchRecommendationsActionNames,
  StartFetchingRecommendations,
  SuccededFetchingRecommendations
} from "../../actions";
import { startFetchingRecommendationsSagas } from "../recommendationSagas";
import { succededFetchingRecommendations } from "../../actionCreators";
import { RecommendationsRequester } from "../../../../requesters/RecommendationsRequester";
import { RecommendationByDay, SocialMediaCountByDay, StockPriceByDay } from "../../../../requesters/CommonDefinitions";
import { StockPriceRequester } from "../../../../requesters/StockPriceRequester";
import { SocialMediaPostCountRequester } from "../../../../requesters/SocialMediaPostCountRequester";
import { put, call } from '@redux-saga/core/effects';
import { FullStockRecommendation, Recommendation } from "../../reducers";

describe('Recommendation Sagas', () => {
  const testDate = moment('05/12/2019', 'MM/DD/YYYY');

  it('SHOULD put succededFetchingRecommendations with empty array WHEN called with empty string stock symbol', () => {
    const action: StartFetchingRecommendations = {
      type: FetchRecommendationsActionNames.FETCHING,
      payload: {
        stockSymbol: '',
        socialMedias: ['Facebook'],
        dates: [testDate],
        recommendationsLoading: true,
      },
    };

    const gen = startFetchingRecommendationsSagas(action);

    expect(gen.next().value).toEqual(put(succededFetchingRecommendations([])));
  });

  it('SHOULD put succededFetchingRecommendations with filled array WHEN called with valid stock symbol', () => {
    const action: StartFetchingRecommendations = {
      type: FetchRecommendationsActionNames.FETCHING,
      payload: {
        stockSymbol: 'abc',
        socialMedias: ['Facebook'],
        dates: [testDate],
        recommendationsLoading: true,
      },
    };

    const stockPricesByDay: StockPriceByDay[] = [
      {
        stockPrice: '9.11',
        date: testDate,
      },
    ];

    const socialMediaPosts: SocialMediaCountByDay[] = [
      {
        numberOfPosts: 1234,
        date: testDate,
      },
    ];

    const recommendationsByDay: RecommendationByDay[] = [
      {
        recommendation: Recommendation.BUY,
        date: testDate,
      },
    ];

    const expectedFullStockRecommendations: FullStockRecommendation[] = [
      {
        date: testDate,
        price: '9.11',
        numberOfPosts: 1234,
        recommendation: Recommendation.BUY,
      },
    ];

    const expectedSucceededAction: SuccededFetchingRecommendations = {
      type: FetchRecommendationsActionNames.SUCCEDED,
      payload: {
        recommendations: expectedFullStockRecommendations,
        recommendationsLoading: false,
      },
    };

    const gen = startFetchingRecommendationsSagas(action);

    const stockPriceRequest = gen.next();
    expect(stockPriceRequest.done).toEqual(false);
    expect(stockPriceRequest.value).toEqual(
      call(StockPriceRequester, action.payload.stockSymbol, action.payload.dates)
    );

    const socialMediaPostQuantityRequest = gen.next(stockPricesByDay);
    expect(socialMediaPostQuantityRequest.done).toEqual(false);
    expect(socialMediaPostQuantityRequest.value).toEqual(
      call(SocialMediaPostCountRequester, action.payload.stockSymbol, action.payload.socialMedias, action.payload.dates)
    );

    // TODO need to test further here
    const recommendationsRequest = gen.next(socialMediaPosts);
    expect(recommendationsRequest.done).toEqual(false);
    expect(recommendationsRequest.value).toEqual(
      call(RecommendationsRequester, ['9.11'], [1234], action.payload.dates)
    );

    const putSuccessAction = gen.next(recommendationsByDay);
    expect(putSuccessAction.done).toEqual(false);
    expect(putSuccessAction.value).toEqual(put(expectedSucceededAction));

  });
});
