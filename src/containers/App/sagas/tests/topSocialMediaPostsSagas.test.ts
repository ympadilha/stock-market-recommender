import moment from 'moment';
import {
	FetchRecommendationsActionNames,
	FetchTopSocialMediaPostsActionNames,
	StartFetchingRecommendations,
	StartFetchingTopSocialMediaPosts, SuccededFetchingTopSocialMediaPosts
} from "../../actions";
import { startFetchingRecommendationsSagas } from "../recommendationSagas";
import { call, put } from '@redux-saga/core/effects';
import { startFetchingTopSocialMediaPostsSagas } from "../topSocialMediaPostsSagas";
import { TopSocialMediaPostsRequester } from "../../../../requesters/TopSocialMediaPostsRequester";
import { succededFetchingRecommendations, succededFetchingTopSocialMediaPosts } from "../../actionCreators";

describe('Top social media post Sagas', () => {
	const testDate = moment('05/12/2019', 'MM/DD/YYYY');

	it('SHOULD put succededFetchingTopSocialMediaPosts with empty array WHEN called with empty string stock symbol', () => {
		const action: StartFetchingRecommendations = {
			type: FetchRecommendationsActionNames.FETCHING,
			payload: {
				stockSymbol: '',
				socialMedias: ['Facebook'],
				dates: [testDate],
			},
		};

		const gen = startFetchingRecommendationsSagas(action);

		expect(gen.next().value).toEqual(put(succededFetchingRecommendations([])));
	});

	it('SHOULD put succededFetchingTopSocialMediaPosts with filled array WHEN called with valid stock symbol', () => {

		const fetchingAction: StartFetchingTopSocialMediaPosts = {
			type: FetchTopSocialMediaPostsActionNames.FETCHING,
			payload: {
				stockSymbol: 'abc',
			},
		};

		const succeededAction: SuccededFetchingTopSocialMediaPosts = {
			type: FetchTopSocialMediaPostsActionNames.SUCCEDED,
			payload: {
				topPosts: [
					{title: 'title', hashTag: 'hashtag', imageUrl: 'imageUrl', description: 'description'}
				]
			},
		};

		const gen = startFetchingTopSocialMediaPostsSagas(fetchingAction);

		const callAPI = gen.next();
		expect(callAPI.done).toEqual(false);
		expect(callAPI.value).toEqual(call(TopSocialMediaPostsRequester, fetchingAction.payload.stockSymbol));

		const posts = gen.next(succeededAction.payload.topPosts);
		expect(posts.done).toEqual(false);
		expect(posts.value).toEqual(put(succededFetchingTopSocialMediaPosts(succeededAction.payload.topPosts)));
	});
});
