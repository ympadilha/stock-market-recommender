import { delay } from 'q';
import { FetchRecommendationsActionNames, StartFetchingRecommendations } from '../actions';
import { hideAlert, showAlert, succededFetchingRecommendations } from '../actionCreators';
import { call, put, takeLatest } from '@redux-saga/core/effects';
import { RecommendationByDay, SocialMediaCountByDay, StockPriceByDay } from '../../../requesters/CommonDefinitions';
import { StockPriceRequester } from '../../../requesters/StockPriceRequester';
import { SocialMediaPostCountRequester } from '../../../requesters/SocialMediaPostCountRequester';
import { RecommendationsRequester } from '../../../requesters/RecommendationsRequester';
import { FullStockRecommendation } from "../reducers";

export function* watchStartFetchingRecommendations() {
  yield takeLatest(FetchRecommendationsActionNames.FETCHING, startFetchingRecommendationsSagas);
}

export function* startFetchingRecommendationsSagas(action: StartFetchingRecommendations) {
  const { stockSymbol, socialMedias, dates } = action.payload;

  if (stockSymbol === '') {
    return yield put(succededFetchingRecommendations([]));
  }

  let stockPricesByDay: StockPriceByDay[];
  let recommendationsByDay: RecommendationByDay[];
  let fullStockRecommendations: FullStockRecommendation[];

  try {
    stockPricesByDay = yield call(StockPriceRequester, stockSymbol, dates);
    const socialMediaPostsQuantityByDay: SocialMediaCountByDay[] = yield call(
      SocialMediaPostCountRequester,
      stockSymbol,
      socialMedias,
      dates
    );

    recommendationsByDay = yield call(
      RecommendationsRequester,
      stockPricesByDay.map(stockPrice => stockPrice.stockPrice),
      socialMediaPostsQuantityByDay.map(post => post.numberOfPosts),
      dates
    );

    fullStockRecommendations = dates.map(date => {
      const price = stockPricesByDay.find(stockPrice => stockPrice.date.isSame(date))!.stockPrice;
      const numberOfPosts = socialMediaPostsQuantityByDay.find(post => post.date.isSame(date))!.numberOfPosts;
      const recommendation = recommendationsByDay.find(recommendation => recommendation.date.isSame(date))!
        .recommendation;

      return {
        date,
        price,
        numberOfPosts,
        recommendation,
      };
    });
  } catch (error) {
    yield put(showAlert());
    yield call(delay, 4000);
    return yield put(hideAlert());
  }

  yield put(succededFetchingRecommendations(fullStockRecommendations));
}
