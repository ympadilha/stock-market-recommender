import { delay } from 'q';
import { call, put, takeLatest } from '@redux-saga/core/effects';
import { FetchTopSocialMediaPostsActionNames, StartFetchingTopSocialMediaPosts } from '../actions';
import { TopSocialMediaPostsOfToday } from '../../../requesters/CommonDefinitions';
import { TopSocialMediaPostsRequester } from '../../../requesters/TopSocialMediaPostsRequester';
import { hideAlert, showAlert, succededFetchingTopSocialMediaPosts } from "../actionCreators";

export function* watchStartFetchingTopSocialMediaPosts() {
  yield takeLatest(FetchTopSocialMediaPostsActionNames.FETCHING, startFetchingTopSocialMediaPostsSagas);
}

export function* startFetchingTopSocialMediaPostsSagas(action: StartFetchingTopSocialMediaPosts) {
  const { stockSymbol } = action.payload;

  if (stockSymbol === '') {
    return yield put(succededFetchingTopSocialMediaPosts([]));
  }

  let topSocialMediaPosts: TopSocialMediaPostsOfToday[];

  try {
    topSocialMediaPosts = yield call(TopSocialMediaPostsRequester, stockSymbol);
  } catch (error) {
    yield put(showAlert());
    yield call(delay, 4000);
    return yield put(hideAlert());
  }

  yield put(succededFetchingTopSocialMediaPosts(topSocialMediaPosts));
}
