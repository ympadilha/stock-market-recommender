import moment from 'moment';
import {
  FetchRecommendationsActionNames,
  StartFetchingRecommendations,
  SuccededFetchingRecommendations
} from "../../actions/recommendationsActions";
import { startFetchingRecommendations, succededFetchingRecommendations } from "../recommendationsActionCreators";
import { mockedRecommendations } from "../../../../components/App/tests/mockedData";

describe('Action Creators', () => {
  it('SHOULD create correct fetching action WHEN fetching action creator is called', () => {
    const testDate = moment.utc('05/12/2019', 'MM/DD/YYYY');
    const expectedStockSymbol = 'ABC';
    const expectedDates = [testDate];
    const expectedSocialMedias = ['Facebook'];

    const expectedPayload: StartFetchingRecommendations = {
      type: FetchRecommendationsActionNames.FETCHING,
      payload: {
        stockSymbol: expectedStockSymbol,
        dates: expectedDates,
        socialMedias: expectedSocialMedias,
        recommendationsLoading: true,
      },
    };

    const action = startFetchingRecommendations(expectedStockSymbol, expectedSocialMedias, expectedDates);
    expect(action).toEqual(expectedPayload);
  });

  it('SHOULD create correct success action WHEN success action creator is called', () => {
    const expectedPayload: SuccededFetchingRecommendations = {
      type: FetchRecommendationsActionNames.SUCCEDED,
      payload: {
        recommendations: mockedRecommendations,
        recommendationsLoading: false,
      },
    };

    const action = succededFetchingRecommendations(mockedRecommendations);
    expect(action).toEqual(expectedPayload);
  });
});

