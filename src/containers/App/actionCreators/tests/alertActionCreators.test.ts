import { hideAlert, showAlert } from '../alertActionCreators';
import { AlertActionNames } from "../../actions";

describe('Alert Action Creators', () => {
  it('SHOULD create correct show action WHEN showAlert is called', () => {
    const createAction = showAlert();
    expect(createAction.type).toEqual(AlertActionNames.SHOW);
  });

  it('SHOULD create correct hide action WHEN hideAlert is called', () => {
    const createAction = hideAlert();
    expect(createAction.type).toEqual(AlertActionNames.HIDE);
  });
});
