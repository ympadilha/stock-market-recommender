import { startFetchingTopSocialMediaPosts, succededFetchingTopSocialMediaPosts } from "../topPostsActionCreators";
import { TopSocialMediaPostsOfToday } from "../../../../requesters/CommonDefinitions";
import { FetchTopSocialMediaPostsActionNames } from "../../actions";

describe('Action Creators', () => {
  it('SHOULD create correct fetching action WHEN startFetchingTopSocialMediaPosts is called', () => {
    const action = startFetchingTopSocialMediaPosts('abc');
    expect(action.type).toEqual(FetchTopSocialMediaPostsActionNames.FETCHING);
    expect(action.payload.stockSymbol).toEqual('abc');
  });

  it('SHOULD create correct success action WHEN succededFetchingTopSocialMediaPosts is called', () => {
    const post: TopSocialMediaPostsOfToday = {
      title: 'title',
      hashTag: 'hashTag',
      imageUrl: 'imageUrl',
      description: 'description',
    };

    const action = succededFetchingTopSocialMediaPosts([post]);
    expect(action.type).toEqual(FetchTopSocialMediaPostsActionNames.SUCCEDED);
    expect(action.payload.topPosts).toEqual([post]);
  });
});

