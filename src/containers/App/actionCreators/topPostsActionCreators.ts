import { TopSocialMediaPostsOfToday } from '../../../requesters';
import {
  FetchTopSocialMediaPostsActionNames,
  StartFetchingTopSocialMediaPosts,
  SuccededFetchingTopSocialMediaPosts
} from "../actions";

export const startFetchingTopSocialMediaPosts = (stockSymbol: string): StartFetchingTopSocialMediaPosts => ({
  type: FetchTopSocialMediaPostsActionNames.FETCHING,
  payload: {
    stockSymbol,
  },
});

export const succededFetchingTopSocialMediaPosts = (
  topPosts: TopSocialMediaPostsOfToday[]
): SuccededFetchingTopSocialMediaPosts => ({
  type: FetchTopSocialMediaPostsActionNames.SUCCEDED,
  payload: {
    topPosts,
  },
});
