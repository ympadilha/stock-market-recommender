import { Moment } from 'moment';
import {
  FetchRecommendationsActionNames,
  StartFetchingRecommendations,
  SuccededFetchingRecommendations,
} from '../actions';
import { FullStockRecommendation } from '../reducers';

export const startFetchingRecommendations = (
  stockSymbol: string,
  socialMedias: string[],
  dates: Moment[]
): StartFetchingRecommendations => ({
  type: FetchRecommendationsActionNames.FETCHING,
  payload: {
    stockSymbol,
    socialMedias,
    dates,
    recommendationsLoading: true,
  },
});

export const succededFetchingRecommendations = (
  recommendations: FullStockRecommendation[]
): SuccededFetchingRecommendations => ({
  type: FetchRecommendationsActionNames.SUCCEDED,
  payload: {
    recommendations,
    recommendationsLoading: false,
  },
});
