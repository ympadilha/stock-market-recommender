import { AlertActionNames, HideAlert, ShowAlert } from "../actions";

export const showAlert = (): ShowAlert => ({
  type: AlertActionNames.SHOW,
});

export const hideAlert = (): HideAlert => ({
  type: AlertActionNames.HIDE,
});
