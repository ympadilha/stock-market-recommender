import React from 'react';
import './alert.css';

interface AlertProps {
	visible?: boolean;
}

export const Alert: React.FC<AlertProps> = ({ visible }) => {

	return (
		<div className={`alert ${visible ? 'alert--visible' : ''}`}>
			Ops, an error has occurred. Please try again.
		</div>
	);

};
