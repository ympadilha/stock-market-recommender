import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { Alert } from "../Alert";

Enzyme.configure({ adapter: new Adapter() });

describe('Alert', () => {
	it('SHOULD match snapshot', () => {
		const wrapper = shallow(<Alert />);
		expect(wrapper).toMatchSnapshot();
	});
});
