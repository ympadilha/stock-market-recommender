import React from 'react';
import './top-posts.css';
import { TopSocialMediaPostsOfToday } from "../../requesters";
import { PostsPlaceholder } from "./components/PostsPlaceholder";

interface TopPostsProps {
	posts: TopSocialMediaPostsOfToday[];
}

export const TopPosts: React.FC<TopPostsProps> = props => {

		const foundPosts = props.posts.length > 0;

		return (
			<div className="topPosts container">
				<h2 className="topPosts__title">Here are the top posts about this:</h2>

				<ul className="topPosts__list">
					{!foundPosts && <PostsPlaceholder />}
					{foundPosts && props.posts.map((post, index) => (
						<li key={index} className="topPosts__item">
							<div className="topPosts__wrapper">
								{/* Not using post.imageUrl here to prevent displaying garbage in the alt */}
								<img className="topPosts__image" alt="" />

								<div className="topPosts__info">
									<h3 className="topPosts__item-title">{post.title}</h3>
									<h4 className="topPosts__item-hashtag">{post.hashTag}</h4>
								</div>
							</div>

							<p className="topPosts__item-text">{post.description}</p>
						</li>
					))}
				</ul>

			</div>
		);
};
