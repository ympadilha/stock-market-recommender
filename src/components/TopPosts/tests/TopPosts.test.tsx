import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { TopPosts } from "../TopPosts";
import { TopSocialMediaPostsOfToday } from "../../../requesters/CommonDefinitions";

Enzyme.configure({ adapter: new Adapter() });

describe('TopPosts snapshot testing', () => {
  const post: TopSocialMediaPostsOfToday = {
    title: 'nice title',
    hashTag: '@nice',
    description: 'nice description',
    imageUrl: 'nice_url',
  };

  it('SHOULD match snapshot WHEN posts are empty', () => {
    const wrapper = shallow(<TopPosts posts={[]} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('SHOULD match snapshot WHEN there are posts', () => {
    const wrapper = shallow(<TopPosts posts={[post]} />);
    expect(wrapper).toMatchSnapshot();
  });
});
