import React from "react";
import './posts-placeholder.css'

export const PostsPlaceholder: React.FC = () => {

	return (
		<div className="posts-placeholder">
			There is no content to show yet. 
			<strong className="posts-placeholder__featured">Search for a stock symbol so we can fetch some.</strong>
		</div>
	);

};