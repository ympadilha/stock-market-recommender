import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { PostsPlaceholder } from "../PostsPlaceholder";

Enzyme.configure({ adapter: new Adapter() });

describe('PostsPlaceholder', () => {
	it('SHOULD match snapshot', () => {
		const wrapper = shallow(
			<PostsPlaceholder/>
		);
		expect(wrapper).toMatchSnapshot();
	});

});