import React from 'react';
import { Table } from '../Table';
import { Form, FormState } from '../Form';
import { Moment } from 'moment';
import moment from 'moment';

import './App.css';
import { TopPosts } from "../TopPosts/TopPosts";
import { TopSocialMediaPostsOfToday } from "../../requesters";
import { Alert } from "../Alert/Alert";
import { FullStockRecommendation } from "../../containers/App/reducers";

interface AppProps {
  recommendations: FullStockRecommendation[];
  topPosts: TopSocialMediaPostsOfToday[];
  startFetchingRecommendations: (stockSymbol: string, socialMedias: string[], dates: Moment[]) => void;
  startFetchingTopSocialMediaPosts: (stockSymbol: string) => void;

  recommendationsLoading: boolean;
  alertIsVisible: boolean;
}

export const App: React.FC<AppProps> = props => {
  const handleChangeForm = (formState: FormState) => {
    const { selectedStockSymbol, selectedSocialMedias, selectedNumberOfPastDays } = formState;

    if (selectedStockSymbol === null) return;

    const generateMomentDates = (pastDays: number): Moment[] => {
      const today = moment();
      const dates: Moment[] = [];

      for (let i = 0; i < pastDays; i++) {
        dates.push(today.clone().subtract(i, 'day'));
      }

      return dates;
    };

    props.startFetchingRecommendations(
      selectedStockSymbol,
      selectedSocialMedias,
      generateMomentDates(selectedNumberOfPastDays)
    );

    props.startFetchingTopSocialMediaPosts(selectedStockSymbol);
  };

  return (
    <div className="app">
      <Alert visible={props.alertIsVisible} />

      <header className="app__header">
        <h1 className="app__main-title">logo</h1>
      </header>

      <section className="app__form-section">
        <Form changeForm={handleChangeForm} />
      </section>

      <section className="app__table-section">
        <Table recommendations={props.recommendations} recommendationsLoading={props.recommendationsLoading} />
      </section>

      <section className="app__top-posts">
        <TopPosts posts={props.topPosts} />
      </section>
    </div>
  );
};
