import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { App } from '../App';
import { mockedRecommendations, mockedTopSocialMediaPosts } from './mockedData';

Enzyme.configure({ adapter: new Adapter() });

describe('App', () => {
  const mockStartFetchingRecommendations = jest.fn();
  const mockStartFetchingTopSocialMediaPosts = jest.fn();

  it('SHOULD match snapshot WHEN recommendations and topSocialMediaPosts are empty', () => {
    const wrapper = shallow(
      <App
        recommendations={[]}
        startFetchingRecommendations={mockStartFetchingRecommendations}
        startFetchingTopSocialMediaPosts={mockStartFetchingTopSocialMediaPosts}
        topPosts={[]}
        alertIsVisible={false}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('SHOULD match snapshot WHEN recommendations and topSocialMediaPosts are filled', () => {
    const wrapper = shallow(
      <App
        recommendations={mockedRecommendations}
        startFetchingRecommendations={mockStartFetchingRecommendations}
        startFetchingTopSocialMediaPosts={mockStartFetchingTopSocialMediaPosts}
        topPosts={mockedTopSocialMediaPosts}
        alertIsVisible={false}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('SHOULD match snapshot WHEN alert is visible', () => {
    const wrapper = shallow(
      <App
        recommendations={mockedRecommendations}
        startFetchingRecommendations={mockStartFetchingRecommendations}
        startFetchingTopSocialMediaPosts={mockStartFetchingTopSocialMediaPosts}
        topPosts={mockedTopSocialMediaPosts}
        alertIsVisible={true}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
