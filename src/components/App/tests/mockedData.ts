import moment from 'moment';
import { FullStockRecommendation, Recommendation } from "../../../containers/App/reducers/recommendationsReducer";
import { TopSocialMediaPostsOfToday } from "../../../requesters/CommonDefinitions";

export const mockedRecommendations: FullStockRecommendation[] = [
  {
    recommendation: Recommendation.BUY,
    numberOfPosts: 2,
    price: '12.12',
    date: moment.utc('05/12/2019', 'MM/DD/YYYY'),
  },
  {
    recommendation: Recommendation.SELL,
    numberOfPosts: 4,
    price: '11.22',
    date: moment.utc('05/17/2019', 'MM/DD/YYYY'),
  },
];

export const mockedTopSocialMediaPosts: TopSocialMediaPostsOfToday[] = [
  { title: 'Agent Smith', description: 'Gotta duplicate, more. Mitosis', hashTag: '@agentpotato', imageUrl: 'agent_smith_url'},
  { title: 'Mr. Anderson', description: 'Dodge bullets, eat hamburguer, cheeseburguer', hashTag: '@andy', imageUrl: 'mr_anderson_url'}
];
