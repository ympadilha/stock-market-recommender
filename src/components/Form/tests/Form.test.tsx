import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { Form } from '../Form';

Enzyme.configure({ adapter: new Adapter() });

describe('Form', () => {
  const changeFormMock = jest.fn();

  it('SHOULD match snapshot WHEN', () => {
    const wrapper = shallow(<Form changeForm={changeFormMock} />);
    expect(wrapper).toMatchSnapshot();
  });
});
