import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { SocialMediaListItem } from '../SocialMediaListItem';

Enzyme.configure({ adapter: new Adapter() });

describe('Form', () => {
  const handleToggleSocialMediaMock = jest.fn();

  it('SHOULD match snapshot WHEN selected is true', () => {
    const wrapper = shallow(
      <SocialMediaListItem
        additionalClassName={''}
        socialMedia={'Instagram'}
        selected={true}
        handleToggleSocialMedia={handleToggleSocialMediaMock}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

	it('SHOULD match snapshot WHEN selected is false', () => {
		const wrapper = shallow(
			<SocialMediaListItem
				additionalClassName={''}
				socialMedia={'Instagram'}
				selected={false}
				handleToggleSocialMedia={handleToggleSocialMediaMock}
			/>
		);
		expect(wrapper).toMatchSnapshot();
	});

	it('SHOULD match snapshot WHEN has additional className', () => {
		const wrapper = shallow(
			<SocialMediaListItem
				additionalClassName={'additionalClassName'}
				socialMedia={'Instagram'}
				selected={false}
				handleToggleSocialMedia={handleToggleSocialMediaMock}
			/>
		);
		expect(wrapper).toMatchSnapshot();
	});
});
