import React, { ChangeEvent } from "react";
import { SocialMedia } from "../../Form";
import './socialMediaListItem.css';

interface SocialMediaListItemProps {
	additionalClassName: string;
	socialMedia: SocialMedia;
	selected: boolean;
	handleToggleSocialMedia: (event: ChangeEvent<HTMLInputElement>, socialMedia: SocialMedia) => void;
}

export const SocialMediaListItem: React.FC<SocialMediaListItemProps> = props => {
	return (
		<li className={`form__social-media-card form__social-media-card--${props.socialMedia} ${props.additionalClassName}`}>
			<label className="form__social-medias-label" htmlFor={`${props.socialMedia}-checkbox`}>
				<span className="form__social-medias-label-text">{props.socialMedia}</span>
			</label>

			<input
				id={`${props.socialMedia}-checkbox`}
				className="form__checkbox"
				type="checkbox"
				checked={props.selected}
				onChange={(event: ChangeEvent<HTMLInputElement>) => props.handleToggleSocialMedia(event, props.socialMedia)}
			/>
		</li>
	);
};
