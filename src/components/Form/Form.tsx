import React, { ChangeEvent } from 'react';
import './form.css';
import { SocialMediaListItem } from "./components";

interface FormProps {
  changeForm: (formState: FormState) => void;
}

export type SocialMedia = 'Facebook' | 'Instagram' | 'Twitter';

export interface FormState {
  selectedStockSymbol: string | null;
  selectedSocialMedias: SocialMedia[];
  selectedNumberOfPastDays: number;
}

export class Form extends React.Component<FormProps, FormState> {
  constructor(props: FormProps) {
    super(props);

    this.state = {
      selectedStockSymbol: null,
      selectedSocialMedias: ['Facebook', 'Instagram', 'Twitter'],
      selectedNumberOfPastDays: 10,
    };
  }

  private handleStockSymbolChange = (event: ChangeEvent<HTMLInputElement>) => {
    const selectedStockSymbol = event.target.value;
    this.setState({ selectedStockSymbol }, () => this.props.changeForm(this.state));
  };

  private handleNumberOfPastDaysChange = (event: ChangeEvent<HTMLInputElement>) => {
    const selectedNumberOfPastDays = parseInt(event.target.value);
    this.setState({ selectedNumberOfPastDays }, () => this.props.changeForm(this.state));
  };

  private handleToggleSocialMedia = (event: ChangeEvent<HTMLInputElement>, socialMedia: SocialMedia) => {
    if (this.state.selectedSocialMedias.includes(socialMedia)) {
      const filtered = this.state.selectedSocialMedias.filter(media => media !== socialMedia);
      this.setState({ selectedSocialMedias: filtered }, () => this.props.changeForm(this.state));
    } else {
      const pushed = [...this.state.selectedSocialMedias, socialMedia];
      this.setState({ selectedSocialMedias: pushed }, () => this.props.changeForm(this.state));
    }
  };

  private paintSocialMedia = (socialMedia: SocialMedia) => {
    const isSelected = this.state.selectedSocialMedias.includes(socialMedia);

    if (!isSelected) return 'form__social-media-card--disabled';
    return '';
  };

  public render = () => {
    const stockSymbol = this.state.selectedStockSymbol ? this.state.selectedStockSymbol : '';

    return (
      <form className="form">
        {/* Can't use fieldset here because of flex
        https://stackoverflow.com/questions/28078681/why-cant-fieldset-be-flex-containers */}
        <div className="form__stockSymbol-wrapper">
          <div className="form__stockSymbol-fieldset container">
            <label className="form__stockSymbol-label" htmlFor="stock-symbol">
              Type in the stock symbol you are looking for:
            </label>
            <input
              id="stock-simbol"
              className="form__stockSymbol-input"
              type="text"
              value={stockSymbol}
              onChange={this.handleStockSymbolChange}
              placeholder="LMI"
            />
          </div>
        </div>
        <div className="form__social-medias container">
          <p className="form__social-medias-title">Select the social medias we will analyse:</p>
          <ul className="form__social-medias-list">
            <SocialMediaListItem
              selected={this.state.selectedSocialMedias.includes('Facebook')}
              socialMedia={'Facebook'}
              handleToggleSocialMedia={this.handleToggleSocialMedia}
              additionalClassName={this.paintSocialMedia('Facebook')}
            />
            <SocialMediaListItem
              selected={this.state.selectedSocialMedias.includes('Instagram')}
              socialMedia={'Instagram'}
              handleToggleSocialMedia={this.handleToggleSocialMedia}
              additionalClassName={this.paintSocialMedia('Instagram')}
            />
            <SocialMediaListItem
              selected={this.state.selectedSocialMedias.includes('Twitter')}
              socialMedia={'Twitter'}
              handleToggleSocialMedia={this.handleToggleSocialMedia}
              additionalClassName={this.paintSocialMedia('Twitter')}
            />
          </ul>
        </div>

        <div className="form__date-range">
          <label className="form__date-range-label" htmlFor="date-range">
            Choose a time range for the analysis:
          </label>
          <input
            id="date-range"
            type="number"
            className="form__date-range-input"
            value={this.state.selectedNumberOfPastDays}
            onChange={this.handleNumberOfPastDaysChange}
          />
        </div>
      </form>
    );
  };
}
