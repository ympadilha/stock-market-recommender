import React from 'react';
import './table.css';
import { FullStockRecommendation } from '../../containers/App/reducers';
import { TableBodyCell } from './components';
import { RecommendationsPlaceholder } from './components/RecommendationsPlaceholder';
import { RecommendationsLoadingModal } from './components/RecommendationsLoadingModal/RecommendationsLoadingModal';

interface TableProps {
  recommendations: FullStockRecommendation[];
  recommendationsLoading: boolean;
}

export const Table: React.FC<TableProps> = props => {
  const foundRecommendations = props.recommendations.length > 0;

  return (
    <div className="table-wrapper">
      {props.recommendationsLoading && <RecommendationsLoadingModal />}

      <table className="table">
        <thead>
          <tr>
            <th className="table__cell table__cell--head">
              <span className="table__cell-inner">Date</span>
            </th>
            <th className="table__cell table__cell--head">
              <span className="table__cell-inner">
                Number
                <br />
                of posts
              </span>
            </th>
            <th className="table__cell table__cell--head">
              <span className="table__cell-inner">
                Stock Price<span className="table__price-currency">(CAD)</span>
              </span>
            </th>
            <th className="table__cell table__cell--head">
              <span className="table__cell-inner">Rating</span>
            </th>
          </tr>
        </thead>
        <tbody className="table__body">
          {!foundRecommendations && <RecommendationsPlaceholder />}
          {foundRecommendations &&
            props.recommendations.map((recommendation, index) => <TableBodyCell {...recommendation} key={index} />)}
        </tbody>
      </table>
    </div>
  );
};
