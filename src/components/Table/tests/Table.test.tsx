import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { Table } from '../Table';
import { mockedRecommendations } from '../../App/tests/mockedData';

Enzyme.configure({ adapter: new Adapter() });

describe('Table', () => {
  it('SHOULD match snapshot WHEN', () => {
    const wrapper = shallow(<Table recommendations={mockedRecommendations} />);
    expect(wrapper).toMatchSnapshot();
  });
});
