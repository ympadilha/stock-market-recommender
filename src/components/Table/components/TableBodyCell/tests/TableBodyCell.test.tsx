import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { TableBodyCell } from "../TableBodyCell";
import moment from "moment";
import { Recommendation } from "../../../../../containers/App/reducers";

Enzyme.configure({ adapter: new Adapter() });

describe('TableBodyCell', () => {

	it('SHOULD match snapshot WHEN', () => {
		const testDate = moment.utc('12/05/2018', 'MM/DD/YYYY');

		const wrapper = shallow(
			<TableBodyCell recommendation={Recommendation.BUY} date={testDate} numberOfPosts={12} price={'12.33'} reactKey={1} />
		);
		expect(wrapper).toMatchSnapshot();
	});


});
