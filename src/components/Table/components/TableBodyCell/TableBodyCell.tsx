import { Recommendation } from '../../../../containers/App/reducers';
import { Moment } from 'moment';
import React from 'react';

interface TableBodyCellProps {
  recommendation: Recommendation;
  date: Moment;
  numberOfPosts: number;
  price: string;
}

export const TableBodyCell: React.FC<TableBodyCellProps> = props => {
  const paintRecommendation = (recommendation: Recommendation) => {
    if (recommendation === Recommendation.BUY) return 'table__cell--buy';
    if (recommendation === Recommendation.SELL) return 'table__cell--sell';
    if (recommendation === Recommendation.KEEP) return 'table__cell--keep';
    return '';
  };

  return (
    <tr className="table__row">
      <td className="table__cell">{props.date.format('MM/DD/YYYY')}</td>
      <td className="table__cell">{props.numberOfPosts}</td>
      <td className="table__cell">{props.price}</td>
      <td className={`table__cell table__cell--recommendation ${paintRecommendation(props.recommendation)}`}>
        {props.recommendation}
      </td>
    </tr>
  );
};
