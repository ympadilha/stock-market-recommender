import React from "react";

export const EmptyTableRow: React.FC = () => (
	<tr className="table__row">
		<td className="table__cell" />
		<td className="table__cell" />
		<td className="table__cell" />
		<td className="table__cell" />
	</tr>
);

