import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { EmptyTableRow } from "../EmptyTableRow";

Enzyme.configure({ adapter: new Adapter() });

describe('EmptyTableRow', () => {
	it('SHOULD match snapshot', () => {
		const wrapper = shallow(<EmptyTableRow />);
		expect(wrapper).toMatchSnapshot();
	});
});
