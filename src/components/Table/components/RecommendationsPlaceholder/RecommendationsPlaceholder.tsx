import React from "react";
import { EmptyTableRow } from "../EmptyTableRow";

export const RecommendationsPlaceholder: React.FC = () => (
	<>
		<EmptyTableRow />
		<EmptyTableRow />
		<EmptyTableRow />
	</>
);
