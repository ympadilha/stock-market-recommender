import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import { RecommendationsPlaceholder } from "../RecommendationsPlaceholder";

Enzyme.configure({ adapter: new Adapter() });

describe('RecommendationsPlaceholder', () => {
	it('SHOULD match snapshot', () => {
		const wrapper = shallow(<RecommendationsPlaceholder />);
		expect(wrapper).toMatchSnapshot();
	});
});
