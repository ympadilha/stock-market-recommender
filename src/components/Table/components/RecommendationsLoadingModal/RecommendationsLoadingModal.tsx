import * as React from "react";
import './recommendationsLoadingModal.css';

export const RecommendationsLoadingModal: React.FC = () => {
	return (
		<div className="recommendations-loading-modal">
			<p className="recommendations-loading-modal__title">Loading statistics</p>
		</div>
	)
};