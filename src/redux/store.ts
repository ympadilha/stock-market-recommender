import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from '@redux-saga/core/effects';
import { recommendationReducer } from "../containers/App/reducers";
import { topSocialMediaPostsReducer } from "../containers/App/reducers";
import { alertReducer } from "../containers/App/reducers";
import { watchStartFetchingTopSocialMediaPosts } from "../containers/App/sagas";
import { watchStartFetchingRecommendations } from "../containers/App/sagas";

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();
const middleware = applyMiddleware(sagaMiddleware);

const rootReducer = combineReducers({ recommendationReducer, topSocialMediaPostsReducer, alertReducer });

// create store
const store = createStore(rootReducer, composeEnhancers(middleware));

function* rootSaga() {
  yield all([watchStartFetchingRecommendations(), watchStartFetchingTopSocialMediaPosts()]);
}
sagaMiddleware.run(rootSaga);

export type AppState = ReturnType<typeof rootReducer>;

// export store singleton instance
export default store;
